<?php

namespace PimAuthTest\Service;

use PimAuth\Entity\User;
use PimAuth\Entity\UserReset;
use PimAuth\Service\UserResetService;
use PimAuthTest\Mock;
use Pimcore\Image\Matrixcode\Renderer\Exception;

/**
 * Class UserResetServiceTest
 *
 * @package PimAuthTest\Service
 */
class UserResetServiceTest extends \PHPUnit_Framework_TestCase
{
    private $events;

    public function setUp()
    {
        \Pimcore::getEventManager()->attach(UserResetService::EVENT_RESET_TOKEN, [$this, 'eventFired']);
    }

    public function tearDown()
    {
    }

    /**
     * Create Reset Token
     */
    public function testCreateResetToken()
    {
        $user = Mock::getUser();
        $userResetService = new UserResetService();
        $userResetService->createResetToken($user);
    }

    /**
     * Get user by token
     *
     * @throws \Exception
     */
    public function testGetUserByToken()
    {
        //Retrieve the test user
        $user = Mock::getUser();
        $userResetService = new UserResetService();

        $userReset = $userResetService->getMapper()->findByUser($user->getId());
        $this->assertEquals(true, ($userReset instanceof UserReset));

        $user = $userResetService->getUserByToken($userReset->getToken());
        $this->assertEquals(true, ($user instanceof User));
    }

    /**
     * Test Token Validity
     */
    public function testTokenValidity()
    {
        $userResetService = new UserResetService();

        //Create a expired token
        $user = Mock::getUser();

        $resetToken = $userResetService->createResetToken($user, new \DateTime('-2 days'));
        $this->assertEquals(false, ($userResetService->isValidToken($resetToken)));

        $userResetService = new UserResetService();
        $resetToken = $userResetService->createResetToken($user);
        $this->assertEquals(true, ($userResetService->isValidToken($resetToken)));
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function testRequestPasswordReset()
    {
        try {
            $invalidated = false;
            $userResetService = new UserResetService();
            $userResetService->requestPasswordReset('in');
            $userResetService->requestPasswordReset(null);
            $userResetService->requestPasswordReset(1);
        } catch (Exception $e) {
            $invalidated = true;
        }
        $this->assertEquals(true, $invalidated);

        $userResetService = new UserResetService();
        $user = Mock::getUser();

        //Request Password Reset
        $userReset = $userResetService->requestPasswordReset($user->getEmail());
        $this->assertEquals(true, ($userReset instanceof UserReset));

        //Event Fire Check
        $this->assertEquals(true, (isset($this->events[UserResetService::EVENT_RESET_TOKEN])));
    }

    /**
     * @param $e
     */
    public function eventFired($e)
    {
        $this->events[$e->getName()] = $e;
    }
}
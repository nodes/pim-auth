<?php

namespace PimAuthTest\Service;

use PimAuth\Service\AuthService;
use PimAuth\Service\UserService;

class AuthServiceTest extends \PHPUnit_Framework_TestCase
{

    const EMAIL = 'exist@oasandbox.info';
    const PASSWORD = 'abCd1234';

    /**
     * @var \PimAuth\Service\UserService
     */
    private $userService;

    /**
     * @var \PimAuth\Service\AuthService
     */
    private $authService;

    /**
     * Initialize setup configurations
     */
    public function setUp()
    {
        $this->userService = new UserService();
        $this->authService = new AuthService($this->userService);
    }

    /**
     * Tear Down
     */
    public function tearDown()
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }
    }

    /**
     * @throws \Exception
     */
    public function testForceLogin()
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }

        $mapper = $this->userService->getMapper();

        if (!$user = $mapper->findByEmail(self::EMAIL)) {
            throw new \Exception('Test user account not exist');
        }

        $this->authService->forceLogin($user);

        $this->assertEquals(true, \Zend_Auth::getInstance()->hasIdentity());
    }

}

<?php

namespace PimAuthTest\Lock;

use BeatSwitch\Lock\Tests\PersistentDriverTestCase;
use PimAuth\Lock\DoctrineDriver;

/**
 * Class DoctrineDriverTest
 */
class DoctrineDriverTest extends PersistentDriverTestCase
{

    public function setUp()
    {
        //Entity Manager
        $em = \Silla\Resource::getEntityManager();

        // Don't forget to reset your DB here.

        // Bootstrap driver.
        $this->driver = new DoctrineDriver($em);

        parent::setUp();
    }
}
<?php

namespace PimAuthTest;

use PimAuth\Entity\User;
use PimAuth\Service\UserService;

class Mock
{

    const EMAIL = 'exist@oasandbox.info';
    const PASSWORD = 'abCd1234';

    /**
     * @return object|User
     * @throws \Exception
     */
    public static function getUser()
    {
        $service = new UserService();
        $user = $service->getMapper()->findByEmail(static::EMAIL);
        if (!$user instanceof User) {
            throw new \Exception('Invalid user entity');
        }
        return $user;
    }

}
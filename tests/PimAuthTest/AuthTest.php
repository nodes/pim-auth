<?php

namespace PimAuthTest;

use PimAuth\Entity\User;
use PimAuth\Service\AuthService;
use PimAuth\Service\UserService;

class AuthTest extends \PHPUnit_Framework_TestCase
{
    private $email;

    /**
     * @var \PimAuth\Service\UserService
     */
    private $userService;
    private $existEmail;
    private $existPass;

    /**
     * @var \PimAuth\Service\AuthService
     */
    private $authService;

    /**
     * @var array
     */
    public $events = [];

    /**
     * Initialize setup configurations
     */
    public function setUp()
    {
        $this->existPass = 'abCd1234';
        $this->existEmail = 'exist@oasandbox.info';

        $this->email = 'mock@oasandbox.info';

        $this->userService = new UserService();
        $this->authService = new AuthService($this->userService);

        //Delete Previous Test Data
        $mapper = $this->userService->getMapper();

        if ($user = $mapper->findByEmail($this->email)) {
            $mapper->delete($user);
        }

        if (!$user = $mapper->findByEmail($this->existEmail)) {
            $user = $mapper->emptyUser();
            $user->setEmail($this->existEmail);
            $user->setPassword($this->userService->encryptPassword($this->existPass));
            $user->setDisplayName('John Exist');
            $user->setState(1);

            $mapper->create($user);
        }

        \Pimcore::getEventManager()->attach(AuthService::EVENT_LOGGED_IN, [$this, 'eventFired']);
        \Pimcore::getEventManager()->attach(AuthService::EVENT_REGISTER, [$this, 'eventFired']);
    }

    /**
     * Tear Down
     */
    public function tearDown()
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }
    }

    /**
     * Verify Password Hash
     */
    public function verifyPasswordHash()
    {
        $mapper = $this->userService->getMapper();
        $user = $mapper->findByEmail($this->existEmail);

        $this->assertEquals(true, $this->userService->verifyPassword($user, $this->existPass));
        $this->assertEquals(false, $this->userService->verifyPassword($user, 'wron=-^'));
        $this->assertEquals(false, $this->userService->verifyPassword($user, ''));
    }

    /**
     * Register Test
     *
     */
    public function testRegistration()
    {
        $v = [
            'email' => $this->email,
            'password' => 'abcd1234',
            'state' => 0,
            'displayName' => 'John Body'
        ];

        try {
            $user = $this->authService->register($v);
        } catch (\Exception $e) {
        }

        $this->assertEquals($user->getEmail(), $v['email']);
        $this->assertEquals($user->getState(), $v['state']);
        $this->assertEquals($user->getDisplayName(), $v['displayName']);
        $this->assertEquals(true, $this->userService->verifyPassword($user, $v['password']));
    }

    /**
     * Test Login
     *
     * @dataProvider providerTestFailedLogin
     *
     * @param $email
     * @param $password
     */
    public function testFailedLogin($email, $password)
    {
        unset($this->events['plugin.pimauth.user.logged_in']);
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }

        $login = $this->authService->loginWithEmail($email, $password);
        $this->assertEquals(false, $login);
    }

    public function providerTestFailedLogin()
    {
        return [
            [$this->existEmail, '12dfdvc#'],
            [$this->existEmail, null],
            [$this->existEmail, ''],
            ['wrong@oasandbox.info', $this->existPass],
            ['wrong@oasandbox.info', ''],
            ['', ''],
            [null, null],
        ];
    }

    /**
     * Test Successful login
     *
     */
    public function testSuccessLogin()
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }

        unset($this->events['plugin.pimauth.user.logged_in']);
        $user = $this->authService->loginWithEmail($this->existEmail, $this->existPass);

        $this->assertEquals(true, ($user instanceof User));
    }

    /**
     * Test Events
     */
    public function testEvents()
    {
        $this->events = [];
        $mapper = $this->userService->getMapper();
        $user = $mapper->findByEmail($this->existEmail);

        $this->authService->triggerLoginEvent($user);
        $this->assertEquals(true, (isset($this->events[AuthService::EVENT_LOGGED_IN])));

        $this->authService->triggerRegisterEvent($user, ['firstName' => 'john']);
        $this->assertEquals(true, (isset($this->events[AuthService::EVENT_REGISTER])));
    }

    /**
     * @param $e
     */
    public function eventFired($e)
    {
        $this->events[$e->getName()] = $e;
    }
}

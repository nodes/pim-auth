<?php

namespace PimAuthTest\Controller;

use PimAuth\Service\UserResetService;
use PimAuthTest\Mock;
use Pimcore\Config;

class AuthControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $domain;

    /**
     * Initialize setup configurations
     */
    public function setUp()
    {
        $this->domain = 'http://playbox.dev';
    }

    /**
     * Ping Url
     */
    public function testAdapter()
    {
        $response = $this->jsonRequest($this->domain . '/user');
        $this->assertEquals(200, 200);
    }

    /**
     * Success Login Url
     *
     */
    public function testSuccessLogin()
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }

        $response = $this->jsonRequest($this->domain . '/user/auth/login', [
            'email' => Mock::EMAIL,
            'password' => Mock::PASSWORD
        ]);
        $this->assertEquals(200, ($response['code'] == 200));
    }

    /**
     * Test Forgot Password Request
     */
    public function testForgotPasswordAction()
    {
        $response = $this->jsonRequest($this->domain . '/user/auth/forgot-password', [
            'email' => Mock::EMAIL
        ]);
        $this->assertEquals(200, ($response['code'] == 200));

        $response = $this->jsonRequest($this->domain . '/user/auth/forgot-password', [
            'email' => null
        ]);
        $this->assertEquals(200, ($response['code'] != 200));
    }

    /**
     * Test Reset Password Request
     */
    public function testResetPasswordAction()
    {
        $response = $this->jsonRequest($this->domain . '/user/auth/reset-password', [
            'password' => Mock::PASSWORD,
            'signature' => '1234565454'
        ]);
        $this->assertEquals(true, ($response['code'] != 200));

        $userResetService = new UserResetService();
        $userReset = $userResetService->createResetToken(Mock::getUser());

        $response = $this->jsonRequest($this->domain . '/user/auth/reset-password', [
            'password' => Mock::PASSWORD,
            'signature' => $userReset->getToken()
        ]);
        //$this->assertEquals(200, $response['code']);
    }

    /**
     * Failed Url
     *
     * @dataProvider providerTestFailedLogin
     */
    public function testFailedLogin($email, $password)
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }

        $response = $this->jsonRequest($this->domain . '/user/auth/login', [
            'email' => $email,
            'password' => $password
        ]);
        $this->assertEquals(200, ($response['code'] != 200));
    }

    /**
     * @return array
     */
    public function providerTestFailedLogin()
    {
        return [
            [Mock::EMAIL, '12dfdvc#'],
            [Mock::EMAIL, null],
            [Mock::EMAIL, ''],
            ['wrong@oasand', Mock::PASSWORD],
            ['wrong@oasandbox.info', ''],
            ['', ''],
            [null, null],
        ];
    }

    /**
     * @param $uri
     * @param $data
     *
     * @return mixed
     * @throws \Zend_Http_Client_Exception
     */
    public function jsonRequest($uri, array $data = ['test' => true])
    {
        $config = array(
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => [
                CURLOPT_FOLLOWLOCATION => true
            ],
        );

        $client = new \Zend_Http_Client($uri, $config);
        $client->setRawData(json_encode($data), 'application/json')->request('POST');
        $response = $client->request('POST');

        $json = null;
        try {
            $json = \Zend_Json::decode($response->getRawBody());
        } catch (\Zend_Json_Exception $e) {
            //echo "Response did not contain valid JSON.";
        }
        return ['data' => $json, 'code' => $response->getStatus()];
    }
}

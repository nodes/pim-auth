<?php
include_once("../../../pimcore/cli/startup.php");
Zend_Session::$_unitTestEnabled = true;

if (!defined("PIMAUTH_TEST_PATH")) {
    define('PIMAUTH_TEST_PATH', realpath(dirname(__FILE__)));
}
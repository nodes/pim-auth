<?php

namespace PimAuth\Entity;

use BeatSwitch\Lock\Callers\Caller;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use PimAuth\Auth;
use Zend\Stdlib\ArraySerializableInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User implements ArraySerializableInterface, Caller
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false,
     *                           unique=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=50, nullable=true)
     */
    protected $displayName;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=256, nullable=false)
     */
    protected $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="smallint", nullable=true)
     */
    protected $state;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50, nullable=true, options={"default" = "member"})
     */
    protected $role;

    /**
     * @ORM\Column(name="metadata", type="json_array", nullable=true)
     */
    private $metadata;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var array
     */
    protected $callerRoles = ['subscriber'];

    /**
     * @var \BeatSwitch\Lock\Callers\CallerLock
     */
    protected $lock;

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return User
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     * @param array $defaults
     * @return User
     */
    public function setMetadata(array $metadata, array $defaults = [])
    {
        $this->metadata = array_merge($defaults, $this->getMetadata(), $metadata);

        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return (!is_array($this->metadata) ? [] : $this->metadata);
    }

    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        $fields = ['email', 'password', 'state', 'role', 'displayName'];
        foreach ($fields as $field) {
            if (isset($array[$field])) {
                $methodName = 'set'.ucfirst($field);
                $this->{$methodName}($array[$field]);
            }
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id' => $this->getId(),
            'displayName' => $this->getDisplayName(),
            'email' => $this->getEmail(),
            'state' => $this->getState(),
            'role' => $this->getRole()
        ];
    }

    /**
     * The type of caller
     *
     * @return string
     */
    public function getCallerType()
    {
        return 'users';
    }

    /**
     * The unique ID to identify the caller with
     *
     * @return int
     */
    public function getCallerId()
    {
        return $this->getId();
    }

    /**
     * The caller's roles
     *
     * @return array
     */
    public function getCallerRoles()
    {
        return $this->callerRoles;
    }

    /**
     * @param $role
     * @return $this
     */
    public function addRole($role)
    {
        if (!in_array($role, $this->callerRoles)) {
            $this->callerRoles[] = $role;
        }

        return $this;
    }

    /**
     * @return \BeatSwitch\Lock\Callers\CallerLock
     */
    public function lock()
    {
        if (null === $this->lock) {
            $manager = Auth::getLockManager();
            $this->lock = $manager->caller($this);
        }

        return $this->lock;
    }
}

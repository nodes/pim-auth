<?php

namespace PimAuth\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="lock_caller_permission")
 */
class CallerPermission
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="caller_type", type="string", length=100, nullable=false)
     */
    protected $callerType;

    /**
     * @var string
     *
     * @ORM\Column(name="caller_id", type="integer", nullable=false)
     */
    protected $callerId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=false)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=100, nullable=false)
     */
    protected $action;

    /**
     * @var string
     *
     * @ORM\Column(name="resource_type", type="string", length=100, nullable=true)
     */
    protected $resourceType;

    /**
     * @var string
     *
     * @ORM\Column(name="resource_id", type="integer", nullable=true)
     */
    protected $resourceId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set callerType
     *
     * @param string $callerType
     * @return CallerPermission
     */
    public function setCallerType($callerType)
    {
        $this->callerType = $callerType;

        return $this;
    }

    /**
     * Get callerType
     *
     * @return string
     */
    public function getCallerType()
    {
        return $this->callerType;
    }

    /**
     * Set callerId
     *
     * @param integer $callerId
     * @return CallerPermission
     */
    public function setCallerId($callerId)
    {
        $this->callerId = $callerId;

        return $this;
    }

    /**
     * Get callerId
     *
     * @return integer
     */
    public function getCallerId()
    {
        return $this->callerId;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return CallerPermission
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return CallerPermission
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set resourceType
     *
     * @param string $resourceType
     * @return CallerPermission
     */
    public function setResourceType($resourceType)
    {
        $this->resourceType = $resourceType;

        return $this;
    }

    /**
     * Get resourceType
     *
     * @return string
     */
    public function getResourceType()
    {
        return $this->resourceType;
    }

    /**
     * Set resourceId
     *
     * @param integer $resourceId
     * @return CallerPermission
     */
    public function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;

        return $this;
    }

    /**
     * Get resourceId
     *
     * @return integer
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'caller_type' => $this->getCallerType(),
            'caller_id' => $this->getCallerId(),
            'type' => $this->getType(),
            'action' => $this->getAction(),
            'resource' => $this->getResourceType(),
            'resource_id' => $this->getResourceId(),
        ];
    }
}
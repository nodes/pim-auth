<?php
use PimAuth\Helper\RequestUtility;
use PimAuth\Service\AuthService;
use PimAuth\Service\UserResetService;
use PimAuth\Service\UserService;

class PimAuth_AuthController extends Zend_Controller_Action
{
    /**
     * @var \PimAuth\Helper\RequestUtility;
     */
    protected $requestUtility;

    public function init()
    {
        $this->requestUtility = new RequestUtility($this->getRequest());

        if (!$this->requestUtility->isJsonHeader()) {
            $this->json('Bad Request', false, 404);
        }
    }

    /**
     * @throws \Exception
     */
    public function loginAction()
    {
        try {
            if (PimAuth\Auth::check()) {
                PimAuth\Auth::logout();
            }

            $params = $this->requestUtility->getJson();

            $userService = new UserService();
            $authService = new AuthService($userService);
            $user = $authService->loginWithEmail($params['email'], $params['password']);

            if (!$user) {
                $this->json('Invalid username or password', false, 401);
            }

            $hydrator = $userService->getHydrator();

            $data = $hydrator->extract($user);
            unset($data['password'], $data['state'], $data['arrayCopy']);

            $this->json($data, true, 200);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 401:
                case 404:
                case 409:
                    $this->json($e->getMessage(), false, $e->getCode());
                    break;

                default:
                    $this->json('Service Temporarily Unavailable', false, 503, $e);
            }
        }
        $this->json('', false, 404);
    }

    /**
     * @throws Exception
     */
    public function logoutAction()
    {
        try {
            if (PimAuth\Auth::check()) {
                PimAuth\Auth::logout();
            }
            $this->json(true, true, 200);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 401:
                case 404:
                case 409:
                    $this->json($e->getMessage(), false, $e->getCode());
                    break;

                default:
                    $this->json('Service Temporarily Unavailable', false, 503, $e);
            }
        }
        $this->json('', false, 404);
    }

    /**
     * @throws \Exception
     */
    public function registerAction()
    {
        try {
            if (PimAuth\Auth::check()) {
                PimAuth\Auth::logout();
            }

            $params = $this->requestUtility->getJson();

            $userService = new UserService();
            $authService = new AuthService($userService);
            $user = $authService->register($params);

            if (!$user) {
                $this->json('Invalid username or password', false, 401);
            }

            $hydrator = $userService->getHydrator();

            $data = $hydrator->extract($user);
            unset($data['password'], $data['state']);

            $this->json($data, true, 200);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 401:
                case 404:
                case 409:
                    $this->json($e->getMessage(), false, $e->getCode());
                    break;

                default:
                    $this->json('Service Temporarily Unavailable', false, 503, $e);
            }
        }
        $this->json('', false, 503);
    }

    /**
     * Ping Action
     */
    public function pingAction()
    {
        try {
            if (PimAuth\Auth::check()) {
                $this->json(true, true, 200);
            } else {
                $this->json('Authentication is needed to get requested response', false, 401);
            }
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 404:
                case 409:
                    $this->json($e->getMessage(), false, $e->getCode());
                    break;

                default:
                    $this->json('Service Temporarily Unavailable', false, 503, $e);
            }
        }
        $this->json('Action aborted', false, 404);
    }

    /**
     * Forgot Password Feature
     */
    public function forgotPasswordAction()
    {
        try {
            $params = $this->requestUtility->getJson();

            $userResetService = new UserResetService();
            $userResetService->requestPasswordReset($params['email']);

            $this->json(true, true, 200);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 401:
                case 404:
                case 409:
                    $this->json($e->getMessage(), false, $e->getCode());
                    break;

                default:
                    $this->json('Service Temporarily Unavailable', false, 503, $e);
            }
        }
        $this->json('', false, 503);
    }

    /**
     * Reset Password Request
     */
    public function resetPasswordAction()
    {
        try {
            $params = $this->requestUtility->getJson();

            $userResetService = new UserResetService();
            $userResetService->changeUserPassword($params['password'], $params['signature']);
            $this->json(true, true, 200);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 401:
                case 404:
                case 409:
                    $this->json($e->getMessage(), false, $e->getCode());
                    break;

                default:
                    $this->json('Service Temporarily Unavailable', false, 503, $e);
            }
        }
        $this->json('', false, 503);
    }

    /**
     * Ping Action
     */
    public function getAction()
    {
        try {
            if (PimAuth\Auth::check()) {
                $user = \PimAuth\Auth::getIdentity();

                $userService = new UserService();
                $hydrator = $userService->getHydrator();
                $data = $hydrator->extract($user);
                unset($data['password'], $data['state'], $data['arrayCopy']);

                if (\PimAuth\Auth::isShadowLogin()) {
                    $realUser = \PimAuth\Auth::getRealIdentity();
                    $data['shadowLogin'] = $realUser->getArrayCopy();
                }

                $this->json($data, true, 200);
            } else {
                $this->json('Authentication is needed to get requested response', false, 401);
            }
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 404:
                case 409:
                    $this->json($e->getMessage(), false, $e->getCode());
                    break;

                default:
                    $this->json('Service Temporarily Unavailable', false, 503, $e);
            }
        }
        $this->json('Action aborted', false, 404);
    }

    /**
     * @param           $data
     * @param bool $success
     * @param int $code
     *
     * @param Exception $e
     *
     * @throws Exception
     * @throws Zend_Controller_Response_Exception
     */
    protected function json($data, $success = false, $code = 500, \Exception $e = null)
    {
        if (is_string($data)) {
            $str = $data;
            $data = [];
            $data['message'] = $str;
        }
        $r = ['success' => $success, 'data' => $data, 'code' => $code];

        if (\Pimcore::inDebugMode()) {
            if (null !== $e) {
                throw new Exception($e);
            }
        }

        $this->getResponse()->setHttpResponseCode($code);
        $this->_helper->json($r);
    }
}
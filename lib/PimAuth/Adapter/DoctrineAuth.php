<?php

namespace PimAuth\Adapter;

use PimAuth\Entity\User;
use PimAuth\Service\UserService;
use Zend_Auth_Adapter_Exception;
use Zend_Auth_Result;

/**
 * Class Doctrine
 *
 * @package PimAuth\Adapter
 */
class DoctrineAuth implements \Zend_Auth_Adapter_Interface
{

    /**
     * @var UserService
     */
    protected $_userService;

    /**
     * @var string
     */
    protected $_username;

    /**
     * @var string
     */
    protected $_password;

    /**
     * Class Constructor
     *
     * @param array $values
     * @param UserService $userService
     *
     */
    public function __construct(array $values = array(), UserService $userService = null)
    {
        if (null === $userService) {
            $this->_userService = new UserService();
        }

        $this->_username = $values['email'];
        $this->_password = $values['password'];
    }

    /**
     * Performs an authentication attempt
     *
     * @throws Zend_Auth_Adapter_Exception If authentication cannot be performed
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        $code = Zend_Auth_Result::FAILURE;
        $identity = null;
        $messages = [];

        $userMapper = $this->_userService->getMapper();

        /** @var User $user */
        $user = $userMapper->findByEmail($this->_username);
        if (!$user) {
            $code = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
        } else {
            if ($user instanceof User) {
                $metadata = $user->getMetadata();

                if ($this->_userService->verifyPassword($user, $this->_password)) {
                    $code = Zend_Auth_Result::SUCCESS;
                    $identity = $user;
                } else if ($metadata['old'] && strlen($user->getPassword()) == 32) {
                    if (md5($this->_password) == $user->getPassword()) {
                        $code = Zend_Auth_Result::SUCCESS;
                        $identity = $user;

                        //Update the database
                        $password = $this->_userService->encryptPassword($this->_password);

                        $em = \Silla\Resource::getEntityManager();
                        $user->setPassword($password);

                        $copy = $metadata['old'];
                        unset($metadata['old']);
                        $metadata['trash_it'] = $copy;
                        $user->setMetadata($metadata);

                        $em->persist($user);
                        $em->flush();
                    }
                } else {
                    $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
                }
            } else {
                $code = Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;
            }
        }
        return $this->result($code, $identity, $messages);
    }

    /**
     * @param       $code
     * @param       $identity
     * @param array $messages
     *
     * @return Zend_Auth_Result
     */
    public function result($code, $identity, $messages = [])
    {
        if (!is_array($messages)) {
            $messages = [$messages];
        }

        return new Zend_Auth_Result(
            $code,
            $identity,
            $messages
        );
    }

}
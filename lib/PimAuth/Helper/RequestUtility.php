<?php

namespace PimAuth\Helper;

/**
 * Class RequestUtility
 *
 * @package PimAuth\Helper
 */
class RequestUtility
{
    /** @var \Zend_Controller_Request_Abstract */
    private $request;

    /**
     * @param \Zend_Controller_Request_Abstract $request
     */
    public function __construct(\Zend_Controller_Request_Abstract $request)
    {
        $this->request = $request;
    }

    /**
     * @return \Zend_Controller_Request_Abstract
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function isJsonHeader()
    {
        if (!$this->getRequest()->getRawBody()) {
            return false;
        }
        return (
        stristr($this->headers('Content-Type'), 'application/json')
            //|| stristr($this->headers('Accept'), 'application/json')
        );
    }

    /**
     * @return mixed
     */
    public function getJson()
    {
        $json = false;
        if ($this->isJsonHeader()) {
            $json = [];
            try {
                $json = \Zend_Json::decode($this->getRequest()->getRawBody());
            } catch (\Zend_Json_Exception $e) {
                //echo "Response did not contain valid JSON.";
            }

        }
        return $json;
    }

    /**
     * Get Header
     *
     * @param null $header
     *
     * @return string
     */
    public function headers($header = null)
    {
        if (function_exists('getallheaders')) {
            $headers = getallheaders();
        } else {
            $headers = '';
            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))]
                        = $value;
                }
            }
        }

        if (empty($headers)) {
            if (function_exists('apache_request_headers')) {
                $headers = apache_request_headers();
                if (isset($headers[$header])) {
                    return $headers[$header];
                }
                $header = strtolower($header);
                foreach ($headers as $key => $value) {
                    if (strtolower($key) == $header) {
                        return $value;
                    }
                }
            }
        }

        if (empty($headers['Content-Type'])) {
            $headers['Content-Type'] = $_SERVER["CONTENT_TYPE"];
        }

        return (null === $header) ? $headers : $headers[$header];
    }
}
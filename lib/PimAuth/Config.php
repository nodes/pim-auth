<?php

namespace PimAuth;

/**
 * Class Config
 *
 * @package PimAuth
 */
class Config
{
    /**
     * @var string
     */
    private $fileLocation;

    /**
     * @var array
     */
    private $options;

    /**
     * Class Constructor
     */
    public function __construct()
    {
        $this->fileLocation = PIMCORE_WEBSITE_VAR
            . '/plugins/pimauth/pimauth-config.php';

        $this->options = $this->_loadConfiguration()['pimauth'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOptions($key = null)
    {
        return (null !== $this->options) ? $this->options[$key] : $this->options;
    }

    /**
     * Get Entity
     */
    public function getEntityName()
    {
        return $this->getOptions('entity');
    }

    /**
     * Register Route
     */
    public static function registerRoutes()
    {
        $front = \Zend_Controller_Front::getInstance();
        $router = $front->getRouter();

        $route = new \Zend_Controller_Router_Route(
            'user/:controller/:action/*',
            array(
                'module' => 'PimAuth',
                "controller" => "auth",
                "action" => "ping"
            )
        );
        $router->addRoute("plugin-pimauth-user", $route);
    }

    /**
     * @throws \Exception
     */
    protected function _loadConfiguration()
    {
        if (\Zend_Registry::isRegistered('pimauth.config')) {
            return \Zend_Registry::get('pimauth.config');
        }

        if (!file_exists($this->fileLocation)) {
            throw new \Exception('pimauth config file not found');
        }

        $config = include $this->fileLocation;
        \Zend_Registry::set('pimauth.config', $config);
        return $config;
    }

}
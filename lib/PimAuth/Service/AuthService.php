<?php

namespace PimAuth\Service;

use PimAuth\Adapter\DoctrineAuth;
use PimAuth\Auth;
use PimAuth\Entity\User;

class AuthService
{

    const EVENT_LOGGED_IN = 'plugin.pimauth.user.logged_in';

    const EVENT_REGISTER = 'plugin.pimauth.user.register';

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * User Service
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService = null)
    {
        if (null === $userService) {
            $userService = new UserService();
        }
        /** @var \PimAuth\Service\UserService $userService */
        $this->userService = $userService;
    }

    /**
     * @return UserService
     */
    public function getUserService()
    {
        return $this->userService;
    }

    /**
     * @param UserService $userService
     * @return AuthService
     */
    public function setUserService($userService)
    {
        $this->userService = $userService;

        return $this;
    }

    /**
     * @param array $values
     *
     * @param array $options
     * @return User
     * @throws \Exception
     */
    public function register(array $values, array $options = [])
    {
        $options = array_merge([
            'state' => 1,
            'role' => 'member',
            'password' => $this->generatePassword()
        ], $options);

        if (!\Zend_Validate::is($values['email'], 'EmailAddress')) {
            throw new \Exception('Only email address can be used as username', 404);
        }

        $mapper = $this->userService->getMapper();
        $user = $mapper->emptyUser();

        //Look for any existing email addresses
        if ($mapper->findByEmail($values['email'])) {
            throw new \Exception('Given email already exist', 409);
        }

        //@todo implement a registration form to take care of the filtering and validation
        if (empty($values['password'])) {
            $values['password'] = $options['password'];
        }

        $user->setEmail($values['email']);
        $user->setPassword($this->userService->encryptPassword(strip_tags($values['password'])));
        $user->setDisplayName(strip_tags($values['displayName']));
        $user->setState($options['state']);
        $user->setRole($options['role']);
        $user->setCreated(new \DateTime('now'));

        if (!$this->userService->verifyPassword($user, $values['password'])) {
            throw new \Exception('ERROR:: Unable to validate the credentials', 401);
        }

        $mapper->create($user);
        $this->triggerRegisterEvent($user, $values);
        return $user;
    }

    /**
     * @param      $email
     * @param      $password
     *
     * @param bool $rememberMe
     *
     * @throws \Exception
     * @throws \Zend_Session_Exception
     * @throws \Zend_Validate_Exception
     * @return bool|\Zend_Auth
     */
    public function loginWithEmail($email, $password, $rememberMe = false)
    {
        if (!\Zend_Validate::is($email, 'EmailAddress')) {
            return false;
        }

        $adapter = new DoctrineAuth(['email' => $email, 'password' => $password]);

        $auth = \Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        if ($result->isValid()) {
            \Zend_Session::regenerateId();
            if ($rememberMe) {
                \Zend_Session::rememberMe(30 * 86400);
            }
            //Allow the app to take control over other things
            $user = $auth->getIdentity();
            $this->triggerLoginEvent($user);
            return $auth->getIdentity();
        }
        return false;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function forceLogin(User $user)
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }

        //Dirty hack to login as some other user
        \Zend_Auth::getInstance()->getStorage()->write($user);
        \Zend_Session::regenerateId();
        return \Zend_Auth::getInstance()->hasIdentity();
    }

    /**
     * @param $user
     * @return \PimAuth\Entity\User
     * @throws \Exception
     */
    public function shadowLogin($user)
    {
        if (!\Zend_Auth::getInstance()->hasIdentity()) {
            throw new \Exception('Unable to login without a valid user session');
        }

        $currentUser = Auth::getRealIdentity();
        $shadow = new \Zend_Session_Namespace('PimAuth_Shadow');
        $shadow->user = $currentUser;

        $shadowUser = $this->forceLogin($user);
        return $shadowUser;
    }

    /**
     * @return \PimAuth\Entity\User
     * @throws \Exception
     */
    public function exitShadowLogin()
    {
        $shadow = new \Zend_Session_Namespace('PimAuth_Shadow');
        $user = $shadow->user;
        if (!$user instanceof User) {
            throw new \Exception('Unable to exit the shadow login');
        }
        \Zend_Session::namespaceUnset('PimAuth_Shadow');

        return $this->forceLogin($user);
    }

    /**
     * @return bool
     */
    public function isShadowLogin()
    {
        if (\Zend_Session::sessionExists()) {
            $shadow = new \Zend_Session_Namespace('PimAuth_Shadow');
            return (isset($shadow->user) && $shadow->user instanceof User);
        }
    }

    /**
     * @return \PimAuth\Entity\User
     */
    public function getRealIdentity()
    {
        if ($this->isShadowLogin()) {
            $shadow = new \Zend_Session_Namespace('PimAuth_Shadow');
            return $shadow->user;
        }
        return \Zend_Auth::getInstance()->getIdentity();
    }

    /**
     * Logout User out of the session
     *
     * @return null
     */
    public function logout()
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }
        return null;
    }

    /**
     * Auto generate a password
     *
     * @param int $length
     * @return null|string
     */
    public function generatePassword($length = 8)
    {
        $password = null;
        $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $charLength = strlen($charset) - 1;

        for ($i = 1; $i <= $length; $i++) {
            $password .= $charset{rand(0, $charLength)};
        }
        return $password;
    }

    /**
     * Trigger Register Event
     *
     * @param User $user
     * @param array $values
     */
    public function triggerRegisterEvent(User $user, array $values = [])
    {
        \Pimcore::getEventManager()->trigger(self::EVENT_REGISTER, $user, ['post' => $values]);
    }

    /**
     * Trigger Login Event
     *
     * @param User $user
     */
    public function triggerLoginEvent(User $user)
    {
        \Pimcore::getEventManager()->trigger(self::EVENT_LOGGED_IN, $user);
    }
}
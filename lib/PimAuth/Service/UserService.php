<?php

namespace PimAuth\Service;

use PimAuth\Config;
use PimAuth\Entity\User;
use PimAuth\Mapper\UserHydrator;
use PimAuth\Mapper\UserMapper;
use PimAuth\Plugin;
use Zend\Crypt\Password\Bcrypt;

/**
 * Class UserService
 *
 * @package PimAuth\Service
 */
class UserService
{
    /**
     * @var
     */
    protected $_mapper;

    /**
     * @var
     */
    protected $_hydrator;

    /**
     * Class Constructor
     *
     * @param Config $config
     */
    public function __construct(Config $config = null)
    {
        $this->_config = (null === $config) ? new Config() : $config;
    }

    /**
     * @param $password
     *
     * @return string
     */
    public function encryptPassword($password)
    {
        $bcrypt = new Bcrypt();
        return $bcrypt->create($password);
    }

    /**
     * @param User $user
     * @param      $password
     *
     * @return bool
     */
    public function verifyPassword(User $user, $password)
    {
        $bcrypt = new Bcrypt();
        return $bcrypt->verify($password, $user->getPassword());
    }

    /**
     * @return \PimAuth\Mapper\UserMapper
     */
    public function getMapper()
    {
        if (null === $this->_mapper) {
            $this->_mapper = new UserMapper(Plugin::getEntityManager(), new Config());
        }
        return $this->_mapper;
    }

    /**
     * @return UserHydrator
     */
    public function getHydrator()
    {
        if (null === $this->_hydrator) {
            $this->_hydrator = new UserHydrator();
        }
        return $this->_hydrator;
    }

    /**
     * @param mixed $hydrator
     */
    public function setHydrator($hydrator)
    {
        $this->_hydrator = $hydrator;
    }

    /**
     * @param $id
     *
     * @return User
     */
    public function findById($id)
    {
        return $this->getMapper()->findById((int)$id);
    }

    /**
     * @param $email
     *
     * @return object|User
     */
    public function findByEmail($email)
    {
        return $this->getMapper()->findByEmail($email);
    }
}
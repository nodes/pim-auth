<?php

namespace PimAuth\Service;

use PimAuth\Entity\User;
use PimAuth\Entity\UserReset;
use PimAuth\Mapper\UserResetMapper;
use Pimcore\Image\Matrixcode\Renderer\Exception;
use PimAuth\Plugin;

class UserResetService
{
    /**
     * This even fires during a request to reset the password via email
     */
    const EVENT_RESET_TOKEN = 'plugin.pimauth.user.password_token';

    /**
     * This even fires during password change
     */
    const EVENT_PASSWORD_CHANGED = 'plugin.pimauth.user.password_changed';

    /**
     * @var
     */
    protected $_mapper;

    /**
     * @var
     */
    protected $_hydrator;

    /**
     * @param $email
     *
     * @return \PimAuth\Entity\UserReset
     * @throws Exception
     * @throws \Exception
     * @throws \Zend_Validate_Exception
     */
    public function requestPasswordReset($email)
    {
        if (!\Zend_Validate::is($email, 'EmailAddress')) {
            throw new Exception('Invalid email address', 404);
        }

        $userService = new UserService();
        $user = $userService->getMapper()->findByEmail($email);

        //Check user existence
        if (!$user) {
            throw new Exception('Requested email does not exist or account has been disabled', 401);
        }

        $resetToken = $this->createResetToken($user);
        if (!$resetToken instanceof UserReset) {
            throw new Exception('Reset password service not available, please try again later!', 500);
        }

        $this->triggerPasswordResetEvent($user, $resetToken);
        return $resetToken;
    }

    /**
     * @param User $user
     *
     * @param \DateTime $date
     *
     * @return UserReset
     */
    public function createResetToken(User $user, \DateTime $date = null)
    {
        $date = ($date === null) ? new \DateTime('now') : $date;
        $mapper = $this->getMapper();

        //First delete any existing tokens
        $userReset = $mapper->findByUser($user->getId());
        if ($userReset) {
            $mapper->delete($userReset);
        }

        //Create a new token
        $token = $mapper->generateToken($user);

        $userReset = $mapper->emptyUserReset();
        $userReset->setUser($user);
        $userReset->setToken($token);
        $userReset->setCreated($date);

        //Create the token
        return $mapper->create($userReset);
    }

    /**
     * Change User Password
     *
     * @param $password
     * @param $token
     *
     * @return User
     * @throws Exception
     */
    public function changeUserPassword($password, $token)
    {
        if (empty($password)) {
            throw new Exception('Password cannot be empty', 404);
        }

        $mapper = $this->getMapper();
        $userToken = $mapper->findByToken($token);

        if (empty($token) || empty($userToken) || !$mapper->hasValidToken($userToken)) {
            throw new Exception('Password reset request has been expired', 401);
        }

        $user = $this->getUserByToken($userToken);

        if (!$user instanceof User) {
            throw new Exception('Sorry unable to complete your request', 500);
        }

        $userService = new UserService();
        $password = $userService->encryptPassword($password);
        $user->setPassword((strip_tags($password)));

        $user = $userService->getMapper()->update($user);

        //Trigger Change
        $this->triggerPasswordChanged($user, $userToken);

        //Invalidate the token immediately
        $mapper->delete($userToken);
        return $user;
    }

    /**
     * @param $token
     *
     * @return bool|User
     */
    public function getUserByToken($token)
    {
        if (!$token instanceof UserReset) {
            $mapper = $this->getMapper();
            $token = $mapper->findByToken($token);
        }

        if (!$token) {
            return false;
        }
        $user = $token->getUser();

        if (!$user) {
            return false;
        }
        return $user;
    }

    /**
     * @param $token
     *
     * @return bool
     */
    public function isValidToken($token)
    {
        if (!$token instanceof UserReset) {
            $mapper = $this->getMapper();
            $token = $mapper->findByToken($token);
        }
        return $this->getMapper()->hasValidToken($token);
    }

    /**
     * @return \PimAuth\Mapper\UserResetMapper
     */
    public function getMapper()
    {
        if (null === $this->_mapper) {
            $this->_mapper = new UserResetMapper(Plugin::getEntityManager());
        }
        return $this->_mapper;
    }

    /**
     * @return null
     */
    public function getHydrator()
    {
        if (null === $this->_hydrator) {
            //$this->_hydrator = new UserHydrator();
        }
        return $this->_hydrator;
    }

    /**
     * @param mixed $hydrator
     */
    public function setHydrator($hydrator)
    {
        $this->_hydrator = $hydrator;
    }

    /**
     * @param User $user
     * @param UserReset $userReset
     */
    public function triggerPasswordResetEvent(User $user, UserReset $userReset)
    {
        \Pimcore::getEventManager()->trigger(self::EVENT_RESET_TOKEN, $user, ['userReset' => $userReset]);
    }

    /**
     * @param User $user
     * @param UserReset $userReset
     */
    public function triggerPasswordChanged(User $user, UserReset $userReset)
    {
        \Pimcore::getEventManager()->trigger(self::EVENT_PASSWORD_CHANGED, $user, ['userReset' => $userReset]);
    }
}
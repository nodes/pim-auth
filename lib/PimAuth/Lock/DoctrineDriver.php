<?php
namespace PimAuth\Lock;

use BeatSwitch\Lock\Callers\Caller;
use BeatSwitch\Lock\Drivers\Driver;
use BeatSwitch\Lock\Permissions\Permission;
use BeatSwitch\Lock\Permissions\PermissionFactory;
use BeatSwitch\Lock\Roles\Role;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use PimAuth\Entity\CallerPermission;
use PimAuth\Entity\RolePermission;

/**
 * Class DoctrineDriver
 */
class DoctrineDriver implements Driver
{
    /**
     * CallerPermission Entity
     */
    const CALLER_ENTITY = '\PimAuth\Entity\CallerPermission';

    /**
     * RolePermission Entity
     */
    const ROLE_ENTITY = '\PimAuth\Entity\RolePermission';

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Returns all the permissions for a caller
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @return \BeatSwitch\Lock\Permissions\Permission[]
     */
    public function getCallerPermissions(Caller $caller)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('c')->from(self::CALLER_ENTITY, 'c');
        $qb->where('c.callerType = :callerType')->setParameter('callerType', $caller->getCallerType());
        $qb->andWhere('c.callerId = :callerId')->setParameter('callerId', $caller->getCallerId());
        $permissions = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        return PermissionFactory::createFromData($permissions);
    }

    /**
     * Stores a new permission into the driver for a caller
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function storeCallerPermission(Caller $caller, Permission $permission)
    {
        $callerPermission = new CallerPermission();
        $callerPermission->setCallerType($caller->getCallerType());
        $callerPermission->setCallerId($caller->getCallerId());
        $callerPermission->setType($permission->getType());
        $callerPermission->setAction($permission->getAction());
        $callerPermission->setResourceType($permission->getResourceType());
        $callerPermission->setResourceId($permission->getResourceId());

        $this->em->persist($callerPermission);
        $this->em->flush();
    }

    /**
     * Removes a permission from the driver for a caller
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function removeCallerPermission(Caller $caller, Permission $permission)
    {
        $permission = $this->em->getRepository(self::CALLER_ENTITY)->findOneBy(
            [
                'callerType' => $caller->getCallerType(),
                'callerId' => $caller->getCallerId(),
                'type' => $permission->getType(),
                'action' => $permission->getAction(),
                'resourceType' => $permission->getResourceType(),
                'resourceId' => $permission->getResourceId(),
            ]
        );

        if ($permission) {
            $this->em->remove($permission);
            $this->em->flush();
        }
    }

    /**
     * Checks if a permission is stored for a user
     *
     * @param \BeatSwitch\Lock\Callers\Caller $caller
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return bool
     */
    public function hasCallerPermission(Caller $caller, Permission $permission)
    {
        $permission = $this->em->getRepository(self::CALLER_ENTITY)->findOneBy(
            [
                'callerType' => $caller->getCallerType(),
                'callerId' => $caller->getCallerId(),
                'type' => $permission->getType(),
                'action' => $permission->getAction(),
                'resourceType' => $permission->getResourceType(),
                'resourceId' => $permission->getResourceId(),
            ]
        );

        return ($permission) ? true : false;
    }

    /**
     * Returns all the permissions for a role
     *
     * @param Role $role
     * @return \BeatSwitch\Lock\Permissions\Permission[]
     */
    public function getRolePermissions(Role $role)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('r')->from(self::ROLE_ENTITY, 'r');
        $qb->where('r.role = :role')->setParameter('role', $role->getRoleName());
        $permissions = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        return PermissionFactory::createFromData($permissions);
    }

    /**
     * Stores a new permission for a role
     *
     * @param Role $role
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function storeRolePermission(Role $role, Permission $permission)
    {
        $rolePermission = new RolePermission();
        $rolePermission->setRole($role->getRoleName());
        $rolePermission->setType($permission->getType());
        $rolePermission->setAction($permission->getAction());
        $rolePermission->setResourceType($permission->getResourceType());
        $rolePermission->setResourceId($permission->getResourceId());
        $this->em->persist($rolePermission);
        $this->em->flush();
    }

    /**
     * Removes a permission for a role
     *
     * @param Role $role
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return void
     */
    public function removeRolePermission(Role $role, Permission $permission)
    {
        $permission = $this->em->getRepository(self::ROLE_ENTITY)->findOneBy(
            [
                'role' => $role->getRoleName(),
                'type' => $permission->getType(),
                'action' => $permission->getAction(),
                'resourceType' => $permission->getResourceType(),
                'resourceId' => $permission->getResourceId(),
            ]
        );

        if ($permission) {
            $this->em->remove($permission);
            $this->em->flush();
        }
    }

    /**
     * Checks if a permission is stored for a role
     *
     * @param Role $role
     * @param \BeatSwitch\Lock\Permissions\Permission
     * @return bool
     */
    public function hasRolePermission(Role $role, Permission $permission)
    {
        $permission = $this->em->getRepository(self::ROLE_ENTITY)->findOneBy(
            [
                'role' => $role->getRoleName(),
                'type' => $permission->getType(),
                'action' => $permission->getAction(),
                'resourceType' => $permission->getResourceType(),
                'resourceId' => $permission->getResourceId(),
            ]
        );

        return ($permission) ? true : false;
    }
}
<?php

namespace PimAuth\Mapper;

use Doctrine\ORM\EntityManager;
use PimAuth\Entity\User;
use PimAuth\Entity\UserReset;

/**
 * Interface UserInterface
 *
 * @package PimAuth\User
 */
class UserResetMapper
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $user
     *
     * @return UserReset
     */
    public function findByUser($user)
    {
        if ($user instanceof User) {
            $user = $user->getId();
        }
        $er = $this->em->getRepository($this->getEntityName());
        return $er->findOneBy(['user_id' => $user]);
    }

    /**
     * @param $token
     *
     * @return \PimAuth\Entity\UserReset
     */
    public function findByToken($token)
    {
        $er = $this->em->getRepository($this->getEntityName());
        return $er->findOneBy(['token' => $token]);
    }

    /**
     * @param User $entity
     *
     * @return string
     */
    public function generateToken(User $entity)
    {
        return strtoupper(
            substr(
                sha1($entity->getId() . '|S*A*L*T|' . time()), 0, 15
            )
        );
    }

    /**
     * @param UserReset $userReset
     *
     * @return bool
     */
    public function hasValidToken(UserReset $userReset)
    {
        $date = new \DateTime('now');
        $now = $date->getTimestamp();

        $date = $userReset->getCreated();
        $date->modify(' +1 day ');
        $tokenTime = $date->getTimestamp();

        return ($tokenTime > $now);
    }

    /**
     * @return UserReset
     */
    public function emptyUserReset()
    {
        return new UserReset();
    }

    /**
     * @param $entity
     *
     * @return \PimAuth\Entity\UserReset
     */
    public function create(UserReset $entity)
    {
        return $this->_persist($entity);
    }

    /**
     * @param $entity
     *
     * @return \PimAuth\Entity\UserReset
     */
    public function update(UserReset $entity)
    {
        return $this->_persist($entity);
    }

    /**
     * @param $entity
     */
    public function delete(UserReset $entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * @param $entity
     *
     * @return \PimAuth\Entity\UserReset
     */
    protected function _persist(UserReset $entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'PimAuth\Entity\UserReset';
    }
}
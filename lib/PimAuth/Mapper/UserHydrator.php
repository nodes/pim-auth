<?php

namespace PimAuth\Mapper;

use PimAuth\Entity\User;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * Class UserHydrator
 *
 * @package PimAuth\Mapper
 */
class UserHydrator extends ClassMethods
{
    /**
     * @param bool $underscoreSeparatedKeys
     */
    public function __construct($underscoreSeparatedKeys = false)
    {
        parent::__construct($underscoreSeparatedKeys);
    }

    /**
     * @param object $object
     *
     * @return array
     * @throws \Exception
     */
    public function extract($object)
    {
        if (!$object instanceof User) {
            throw new \Exception('$object must be an instance of PimAuth\Entity\User');
        }
        /* @var $object UserInterface */
        $data = parent::extract($object);
        return $data;
    }

    /**
     * @param array $data
     * @param object $object
     *
     * @return object
     * @throws \Exception
     */
    public function hydrate(array $data, $object)
    {
        if (!$object instanceof User) {
            throw new \Exception('$object must be an instance of PimAuth\Entity\User');
        }
        //$data = $this->mapField('user_id', 'id', $data);
        return parent::hydrate($data, $object);
    }

    /**
     * @param       $keyFrom
     * @param       $keyTo
     * @param array $array
     *
     * @return array
     */
    protected function mapField($keyFrom, $keyTo, array $array)
    {
        $array[$keyTo] = $array[$keyFrom];
        unset($array[$keyFrom]);
        return $array;
    }
}

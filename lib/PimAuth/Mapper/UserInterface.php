<?php

namespace PimAuth\Mapper;

/**
 * Interface UserInterface
 *
 * @package PimAuth\Entity
 */
interface UserInterface
{
    /**
     * Finds a user by the given user ID.
     *
     * @param int $id
     *
     * @return \PimAuth\Entity\User
     */
    public function findById($id);

    /**
     * Finds a user by the given credentials.
     *
     * @param array $credentials
     *
     * @return \PimAuth\Entity\User
     */
    public function findByCredentials(array $credentials);

    /**
     * Returns an all users.
     *
     * @return array
     */
    public function findAll();
}
<?php

namespace PimAuth\Mapper;

use Doctrine\ORM\EntityManager;
use Pimauth\Config;
use PimAuth\Entity\User;

/**
 * Interface UserInterface
 *
 * @package PimAuth\User
 */
class UserMapper implements UserInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \Pimauth\Config
     */
    protected $config;

    /**
     * @param EntityManager $em
     * @param array|Config $config
     */
    public function __construct(EntityManager $em, Config $config)
    {
        $this->em = $em;
        $this->config = $config;
    }

    /**
     * @param $email
     *
     * @return \PimAuth\Entity\User|object
     */
    public function findByEmail($email)
    {
        $er = $this->em->getRepository($this->config->getEntityName());
        return $er->findOneBy(array('email' => $email));
    }

    /**
     * Finds a user by the given user ID.
     *
     * @param int $id
     *
     * @return \PimAuth\Entity\User
     */
    public function findById($id)
    {
        $er = $this->em->getRepository($this->config->getEntityName());
        return $er->find($id);
    }

    /**
     * Finds a user by the given credentials.
     *
     * @param array $credentials
     *
     * @return \PimAuth\Entity\User
     */
    public function findByCredentials(array $credentials)
    {
        $er = $this->em->getRepository($this->config->getEntityName());
        return $er->findOneBy($credentials);
    }

    /**
     * @return User
     */
    public function emptyUser()
    {
        return new User();
    }

    /**
     * Returns an all users.
     *
     * @return array
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }

    /**
     * @param $entity
     *
     * @return \PimAuth\Entity\User
     */
    public function create($entity)
    {
        return $this->_persist($entity);
    }

    /**
     * @param $entity
     *
     * @return \PimAuth\Entity\User
     */
    public function update($entity)
    {
        return $this->_persist($entity);
    }

    /**
     * @param $entity
     */
    public function delete($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * @param $entity
     *
     * @return \PimAuth\Entity\User
     */
    protected function _persist($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }
}
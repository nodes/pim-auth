<?php

namespace PimAuth;

use BeatSwitch\Lock\Manager;
use PimAuth\Entity\User;
use PimAuth\Lock\DoctrineDriver;
use PimAuth\Service\AuthService;

/**
 * Class Auth
 *
 * @package PimAuth
 */
class Auth
{
    /**
     * @var AuthService
     */
    private static $service;

    private static $lockManager;

    /**
     * @return Manager
     */
    public static function getLockManager()
    {
        if (null === self::$lockManager) {
            $doctrineDriver = new DoctrineDriver(Plugin::getEntityManager());
            self::$lockManager = new Manager($doctrineDriver);
        }

        return self::$lockManager;
    }

    /**
     * @return bool
     */
    public static function check()
    {
        return \Zend_Auth::getInstance()->hasIdentity();
    }

    /**
     * @param array $values
     */
    public static function createUser(array $values = [])
    {
        $userService = static::getServiceInstance()->getUserService();

        $user = new User();
        $user->setEmail($values['email']);
        $user->setPassword($userService->encryptPassword($values['password']));
        $user->setDisplayName(strip_tags($values['displayName']));
        $user->setState($values['state']);
        $user->setRole($values['role']);
        $user->setCreated(new \DateTime('now'));
    }

    /**
     * @return mixed|null
     */
    public static function getIdentity()
    {
        return \Zend_Auth::getInstance()->getIdentity();
    }

    /**
     * Logout the user
     */
    public static function logout()
    {
        if (static::isShadowLogin()) {
            static::exitShadowLogin();
        }
        return \Zend_Auth::getInstance()->clearIdentity();
    }

    /**
     * @param User $user
     * @return bool
     */
    public static function forceLogin(User $user)
    {
        return static::getServiceInstance()->forceLogin($user);
    }

    /**
     * @param array $values
     * @param array $options
     * @return User
     * @throws \Exception
     */
    public static function register(array $values, $options = [])
    {
        return static::getServiceInstance()->register($values, $options);
    }

    /**
     * @param $email
     * @param $password
     * @param bool $rememberMe
     * @return bool|\Zend_Auth
     */
    public static function login($email, $password, $rememberMe = false)
    {
        return static::getServiceInstance()->loginWithEmail($email, $password, $rememberMe);
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public static function shadowLogin(User $user)
    {
        return static::getServiceInstance()->shadowLogin($user);
    }

    /**
     * @return User
     */
    public static function getRealIdentity()
    {
        return static::getServiceInstance()->getRealIdentity();
    }

    /**
     * @return User
     * @throws \Exception
     */
    public static function exitShadowLogin()
    {
        return static::getServiceInstance()->exitShadowLogin();
    }

    /**
     * @return bool
     */
    public static function isShadowLogin()
    {
        return static::getServiceInstance()->isShadowLogin();
    }

    /**
     * @return AuthService
     */
    public static function getServiceInstance()
    {
        if (null === static::$service) {
            static::$service = new AuthService();
        }
        return static::$service;
    }

    /**
     * @return Service\UserService
     */
    public static function getUserService()
    {
        return static::getServiceInstance()->getUserService();
    }

    /**
     * @return AuthService
     */
    public static function getAuthService()
    {
        if (null === static::$service) {
            static::$service = new AuthService();
        }

        return static::$service;
    }
}
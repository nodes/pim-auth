<?php
namespace PimAuth;

use Pimcore\API\Plugin as PluginLib;

class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    /**
     * @var Config
     */
    public static $config;

    /**
     * Init
     */
    public function init()
    {
        static::$config = new Config();
        static::$config->registerRoutes();
    }

    /**
     * @return bool|string
     */
    public static function install()
    {
        $path = self::getInstallPath();

        if (!is_dir($path)) {
            mkdir($path);
        }

        if (self::isInstalled()) {
            return "PimAuth Plugin successfully installed.";
        } else {
            return "PimAuth Plugin could not be installed";
        }
    }

    /**
     * @return string
     */
    public static function getInstallPath()
    {
        return static::pluginPath() . '/data/installed';
    }

    /**
     * Get Plugin Path
     *
     * @return string
     */
    public static function pluginPath()
    {
        return PIMCORE_PLUGINS_PATH . '/PimAuth';
    }

    /**
     * @return bool
     */
    public static function isInstalled()
    {
        return is_dir(static::getInstallPath());
    }

    /**
     * @return string
     */
    public static function uninstall()
    {
        rmdir(self::getInstallPath());

        if (!self::isInstalled()) {
            return "PimAuth Plugin successfully uninstalled.";
        } else {
            return "PimAuth Plugin could not be uninstalled";
        }
    }

    /**
     * Get Entity Manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public static function getEntityManager()
    {
        $container = \Zend_Controller_Front::getInstance()->getParam('ServiceContainer');
        return $container->get('vein.entity_manager');
    }
}